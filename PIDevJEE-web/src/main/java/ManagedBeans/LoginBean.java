package ManagedBeans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import model.Candidate;
import services.CandidateService;

@ManagedBean
@SessionScoped
public class LoginBean implements Serializable {

	private String email; 
	private String password;
	private Candidate candidat;
	private Boolean loggedIn;
	public static int idconn = 0;

	
	public Boolean getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public Candidate getCandidat() {
		return candidat;
	}
	public void setCandidat(Candidate candidat) {
		this.candidat = candidat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@EJB
	CandidateService SCand;
	
	public String Login()
	{
		String navigateTo = "null"; 
		candidat = SCand.getByEmailPwd(email, password); 

		if (candidat != null ) {
			idconn = candidat.getId();
			System.out.println("*******************************************");
			System.out.println("id connecté  "+idconn);
			CandidateBean.setCandidatconnect(SCand.DisplayCandidate(idconn));
			navigateTo = "/pages/candidate/profilCandidate?faces-redirect=true";
			loggedIn = true; 
		}
		else 
		{
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad Credentials"));
		}
		return navigateTo; 
	}
}
