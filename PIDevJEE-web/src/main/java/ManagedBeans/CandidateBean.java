package ManagedBeans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import model.Candidate;
import model.Favorite;
import model.JobOffer;

import javax.ejb.EJB;

import services.*;

@ManagedBean(name = "candidateBean")
@SessionScoped
public class CandidateBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private int act;
	private String address;
	private String creationDate;
	private String email;
	private String firstName;
	private String introduction;
	private String lastName;
	private String password;
	private String phoneNumber;
	private String photo;
	private String skills;
	private String userName;
	private Candidate candidat;
	public static Candidate candidatconnect = null;
	private List<JobOffer> result;
	private List<Favorite> resultfavs;
	private boolean exist = false;
	private boolean disable = false;

	@EJB
	CandidateService SCand;

	public CandidateBean() {
		// super();
		System.out.println("*******************************************");
		System.out.println("id connecté  " + LoginBean.idconn);
		this.setCandidat(candidatconnect);
		System.out.println("*******************************************");
		System.out.println("candidat  " + candidatconnect.getEmail());

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<JobOffer> getResult() {
		return result;
	}

	public void setResult(List<JobOffer> result) {
		this.result = result;
	}

	public Candidate getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidate candidat) {
		this.candidat = candidat;
	}

	public static Candidate getCandidatconnect() {
		return candidatconnect;
	}

	public static void setCandidatconnect(Candidate candidatconnect) {
		CandidateBean.candidatconnect = candidatconnect;
	}

	public Candidate getCandidate(int id) {
		candidat = SCand.DisplayCandidate(id);
		System.out.println("candidat candidateBean " + candidat.getEmail());
		return candidat;
	}

	public String getMatchingJobs() {
		String navigateTo = "null";

		result = SCand.matchingSkills(LoginBean.idconn, SCand.getKeywordsForJob());

		if (result.isEmpty() == false) {
			navigateTo = "/pages/candidate/matchingJobOffers?faces-redirect=true";
			SCand.sendMail(candidatconnect.getEmail(), result);

		} else {
			navigateTo = "/pages/candidate/noMatchingJobOffers?faces-redirect=true";
		}
		return navigateTo;
	}

	public List<JobOffer> getJobsToDisplay() {
		result = SCand.matchingSkills(LoginBean.idconn, SCand.getKeywordsForJob());
		List<JobOffer> resultWithValidDate = new ArrayList<>();

		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dateSys = dateFormat.parse(LocalDate.now().toString());
			for (int i = 0; i < result.size(); i++) {
				Date dateValid = dateFormat.parse(result.get(i).getValidityDate().toString());
				if (dateSys.compareTo(dateValid) < 0) {
					resultWithValidDate.add(result.get(i));
				}
			}
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}

		result = resultWithValidDate;
		return result;
	}

	public void removeFavoritesCandidat(int idCand, int idJob) {
		resultfavs = SCand.allFavorites();
		for (int i = 0; i < resultfavs.size(); i++) {
			if (resultfavs.get(i).getCandidate().getId() == idCand
					&& resultfavs.get(i).getJoboffer().getId() == idJob) {
				SCand.deleteFavorite(resultfavs.get(i).getId());
			}
		}
	}

	public void addFavoritesCandidate(JobOffer job) {
		SCand.addFavorite(new Favorite(SCand.DisplayCandidate(LoginBean.idconn), job));
	}

	public boolean favExist() {
		return exist;
	}

	public boolean getAllFavsForCandidate(int idCand, int idJob) {
		boolean var = false;
		resultfavs = SCand.allFavorites();
		for (int i = 0; i < resultfavs.size(); i++) {
			if (resultfavs.get(i).getCandidate().getId() == idCand
					&& resultfavs.get(i).getJoboffer().getId() == idJob) {
				var= true;
			}
			else {
				var= false;
			}
		}
		return var;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

}
