package services;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.CandidateServiceLocal;
import interfaces.CandidateServiceRemote;
import model.Candidate;
import model.Favorite;
import model.JobOffer;

@Stateless
@LocalBean
public class CandidateService implements CandidateServiceLocal, CandidateServiceRemote {
	
	
	@PersistenceContext
	EntityManager em;

	@Override
	public Candidate DisplayCandidate(int id) {
		System.out.println("****************"+em.find(Candidate.class, id).getEmail());
		TypedQuery<Candidate> query =  em.createQuery("Select c from Candidate c WHERE c.id=:id", Candidate.class);
		query.setParameter("id", id); 
		Candidate candidat = null; 
		try {
			candidat = query.getSingleResult(); 
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return candidat;
	}

	@Override
	public Candidate getByEmailPwd(String email, String pwd) {
		TypedQuery<Candidate> query = 
				em.createQuery("select c from Candidate c WHERE c.email=:email and c.password=:pwd ", Candidate.class); 
				query.setParameter("email", email); 
				query.setParameter("pwd", pwd); 
				Candidate candidat = null; 
				try {
					candidat = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return candidat;
	}
	
	
	@Override
	public List<JobOffer> getKeywordsForJob (){
		
		TypedQuery<JobOffer> query =  em.createQuery("SELECT j FROM JobOffer j", JobOffer.class);
		List<JobOffer> allkeywords = null;
		
		try {
			allkeywords=query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		
		return allkeywords;	
	}
	
	
	@Override
	public List<JobOffer> matchingSkills (int id, List<JobOffer> keywords) {
		
		List<JobOffer> jobOffersThatMatch = new ArrayList<>();
		String allskills = null;
		
		TypedQuery<Candidate> query =  em.createQuery("Select c from Candidate c WHERE c.id=:id", Candidate.class);
		query.setParameter("id", id); 
		Candidate candidat = null; 
		
		try {
			candidat = query.getSingleResult(); 
			allskills = candidat.getSkills();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
			
			for (int i = 0; i < keywords.size(); i++) {
				System.out.println("first for : " + i);
				String searchkey=keywords.get(i).getKeyword();
				if (allskills.contains(searchkey)) {
					System.out.println("if (allskills.contains(searchkey)) ");
					
						jobOffersThatMatch.add(keywords.get(i));
					
				}
			}
	
		return jobOffersThatMatch ;
		
	}
	
	
	
	@Override
	public void sendMail (String recepient, List<JobOffer> jobOffersList) {
		
		System.out.println("Preparing to send mail");
		
		Properties properties = new Properties();
		
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
		
		String myAccountEmail = "findmeajobonlineservices@gmail.com";
		String password = "findmeajob123online";
		
		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myAccountEmail, password);
			}
		}); 
		
		Message message = prepareMessage(session,myAccountEmail,recepient,jobOffersList);
		
		try {
			Transport.send(message);
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		System.out.println("Message sent successfully");
		
	}
	
	private static Message prepareMessage(Session session , String myAccountEmail, String recepient,List<JobOffer> jobOffersList) {
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dateSys = dateFormat.parse(LocalDate.now().toString());
			String descjobs =  "Please check the following job offer(s) : \n" ;
			
			for (int i = 0; i < jobOffersList.size(); i++) {
				Date dateValid = dateFormat.parse(jobOffersList.get(i).getValidityDate().toString());
				if (dateSys.compareTo(dateValid) < 0) {
					long nbOfDaysBetween = ChronoUnit.DAYS.between(LocalDate.now(),dateValid.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
					descjobs = descjobs + jobOffersList.get(i).getDescription() + " : You have "+nbOfDaysBetween+" days left to apply for this job" + " , \n";
				}
			}
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myAccountEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recepient));
			message.setSubject("Job offer match from FindMeAJob");
			message.setText("Hello, \n"
					+ "We, the FindMeAJob team , found a perfect match for you based on your skills ! \n"
					+descjobs);
			return message;
			
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		return null;
		
	}
	
	
	
	@Override
	public List<Favorite> allFavorites (){
		
		TypedQuery<Favorite> query =  em.createQuery("SELECT j FROM Favorite j", Favorite.class);
		List<Favorite> allfavs = null;
		
		try {
			allfavs=query.getResultList();
		}
		catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		
		return allfavs;	
	}
	
	
	
	@Override
	public void addFavorite(Favorite f) {
		em.persist(f);	
	}
	
	
	@Override
	public void deleteFavorite(int id) {
		Favorite f = em.find(Favorite.class, id);
		em.remove(f);
		
	}
//	
//	
//	@Override
//	public Employe getEmployeByEmailAndPassword(String nom, String prenom) {
//		// TODO Auto-generated method stub
//		TypedQuery<Employe> query=em.createQuery("Select e from Employe e where e.nom=:nom AND e.prenom=:prenom",Employe.class);
//		query.setParameter("nom", nom);
//		query.setParameter("prenom", prenom);
//		Employe e=null;
//		try {
//			e=query.getSingleResult();
//		}catch (Exception x) {
//			System.out.println("error="+x);
//		}
//		return e;
//	}

}
