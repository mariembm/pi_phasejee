package interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.*;

@Remote
public interface CandidateServiceRemote {

	public Candidate DisplayCandidate (int id);
	public Candidate getByEmailPwd (String email, String pwd); 
	public List<JobOffer> matchingSkills (int id, List<JobOffer> keywords);
	public List<JobOffer> getKeywordsForJob();
	public void sendMail (String recepient, List<JobOffer> jobOffersList);
	public void addFavorite(Favorite f);
	public void deleteFavorite(int id);
	public List<Favorite> allFavorites ();
}
