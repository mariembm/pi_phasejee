package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Contacts database table.
 * 
 */
@Embeddable
public class ContactPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="SenderId")
	private int senderId;

	@Column(name="ReceiverId")
	private int receiverId;

	@Column(name="DateEnvoie")
	private String dateEnvoie;

	public ContactPK() {
	}
	public int getSenderId() {
		return this.senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public int getReceiverId() {
		return this.receiverId;
	}
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	public String getDateEnvoie() {
		return this.dateEnvoie;
	}
	public void setDateEnvoie(String dateEnvoie) {
		this.dateEnvoie = dateEnvoie;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ContactPK)) {
			return false;
		}
		ContactPK castOther = (ContactPK)other;
		return 
			(this.senderId == castOther.senderId)
			&& (this.receiverId == castOther.receiverId)
			&& this.dateEnvoie.equals(castOther.dateEnvoie);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.senderId;
		hash = hash * prime + this.receiverId;
		hash = hash * prime + this.dateEnvoie.hashCode();
		
		return hash;
	}
}