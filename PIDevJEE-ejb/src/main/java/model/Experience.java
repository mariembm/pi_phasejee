package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Experiences database table.
 * 
 */
@Entity
@Table(name="Experiences")
@NamedQuery(name="Experience.findAll", query="SELECT e FROM Experience e")
public class Experience implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;

	private String campany;

	private String description;

	private String endDate;

	private String startDate;

	private String workPlace;

	//bi-directional many-to-one association to Candidate
	@ManyToOne
	@JoinColumn(name="CandidatId")
	private Candidate candidate;

	public Experience() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCampany() {
		return this.campany;
	}

	public void setCampany(String campany) {
		this.campany = campany;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEndDate() {
		return this.endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getWorkPlace() {
		return this.workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public Candidate getCandidate() {
		return this.candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

}