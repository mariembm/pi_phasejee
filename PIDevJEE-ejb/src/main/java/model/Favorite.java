package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Favorites
 *
 */
@Entity
@Table(name = "Favorites")
@NamedQuery(name = "Favorite.findAll", query = "SELECT f FROM Favorite f")
public class Favorite implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int Id;

	// bi-directional many-to-one association to Candidate
	@ManyToOne
	@JoinColumn(name = "CandidateId")
	private Candidate candidate;

	// bi-directional many-to-one association to Candidate
	@ManyToOne
	@JoinColumn(name = "JobOfferId")
	private JobOffer joboffer;

	public Favorite() {
		super();
	}
	

	public Favorite(Candidate candidate, JobOffer joboffer) {
		super();
		this.candidate = candidate;
		this.joboffer = joboffer;
	}



	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public JobOffer getJoboffer() {
		return joboffer;
	}

	public void setJoboffer(JobOffer joboffer) {
		this.joboffer = joboffer;
	}
	
	

}
