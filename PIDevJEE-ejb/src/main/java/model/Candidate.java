package model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;

/**
 * The persistent class for the Candidates database table.
 * 
 */
@Entity
@Table(name = "Candidates")
@NamedQuery(name = "Candidate.findAll", query = "SELECT c FROM Candidate c")
public class Candidate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int id;

	private int act;

	private String address;

	private String creationDate;

	private String email;

	private String firstName;

	private String introduction;

	private String lastName;

	private String password;

	private String phoneNumber;

	private String photo;

	private String skills;

	private String userName;

	// bi-directional many-to-many association to Candidate
	@ManyToMany
	@JoinTable(name = "CandidateCandidates", joinColumns = {
			@JoinColumn(name = "Candidate_Id1") }, inverseJoinColumns = { @JoinColumn(name = "Candidate_Id") })
	private List<Candidate> candidates1;

	// bi-directional many-to-many association to Candidate
	@ManyToMany(mappedBy = "candidates1")
	private List<Candidate> candidates2;

	// bi-directional many-to-one association to Certification
	@OneToMany(mappedBy = "candidate")
	private List<Certification> certifications;

	// bi-directional many-to-one association to Education
	@OneToMany(mappedBy = "candidate")
	private List<Education> educations;

	// bi-directional many-to-one association to Experience
	@OneToMany(mappedBy = "candidate")
	private List<Experience> experiences;

	// bi-directional many-to-one association to Favorite
	@OneToMany(mappedBy = "candidate")
	private List<Favorite> favorites;

	public Candidate() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAct() {
		return this.act;
	}

	public void setAct(int act) {
		this.act = act;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getSkills() {
		return this.skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Candidate> getCandidates1() {
		return this.candidates1;
	}

	public void setCandidates1(List<Candidate> candidates1) {
		this.candidates1 = candidates1;
	}

	public List<Candidate> getCandidates2() {
		return this.candidates2;
	}

	public void setCandidates2(List<Candidate> candidates2) {
		this.candidates2 = candidates2;
	}

	public List<Certification> getCertifications() {
		return this.certifications;
	}

	public void setCertifications(List<Certification> certifications) {
		this.certifications = certifications;
	}

	public Certification addCertification(Certification certification) {
		getCertifications().add(certification);
		certification.setCandidate(this);

		return certification;
	}

	public Certification removeCertification(Certification certification) {
		getCertifications().remove(certification);
		certification.setCandidate(null);

		return certification;
	}

	public List<Education> getEducations() {
		return this.educations;
	}

	public void setEducations(List<Education> educations) {
		this.educations = educations;
	}

	public Education addEducation(Education education) {
		getEducations().add(education);
		education.setCandidate(this);

		return education;
	}

	public Education removeEducation(Education education) {
		getEducations().remove(education);
		education.setCandidate(null);

		return education;
	}

	public List<Experience> getExperiences() {
		return this.experiences;
	}

	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}

	public Experience addExperience(Experience experience) {
		getExperiences().add(experience);
		experience.setCandidate(this);

		return experience;
	}

	public Experience removeExperience(Experience experience) {
		getExperiences().remove(experience);
		experience.setCandidate(null);

		return experience;
	}

	public List<Favorite> getFavorites() {
		return favorites;
	}

	public void setFavorites(List<Favorite> favorites) {
		this.favorites = favorites;
	}
	
	

}