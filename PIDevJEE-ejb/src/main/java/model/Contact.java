package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the Contacts database table.
 * 
 */
@Entity
@Table(name="Contacts")
@NamedQuery(name="Contact.findAll", query="SELECT c FROM Contact c")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ContactPK id;

	private int accept;

	public Contact() {
	}

	public ContactPK getId() {
		return this.id;
	}

	public void setId(ContactPK id) {
		this.id = id;
	}

	public int getAccept() {
		return this.accept;
	}

	public void setAccept(int accept) {
		this.accept = accept;
	}

}