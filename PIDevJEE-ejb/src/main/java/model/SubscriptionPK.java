package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the Subscriptions database table.
 * 
 */
@Embeddable
public class SubscriptionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="FollowerId")
	private int followerId;

	@Column(name="EntrepriseId")
	private int entrepriseId;

	@Column(name="DateEnvoie")
	private String dateEnvoie;

	public SubscriptionPK() {
	}
	public int getFollowerId() {
		return this.followerId;
	}
	public void setFollowerId(int followerId) {
		this.followerId = followerId;
	}
	public int getEntrepriseId() {
		return this.entrepriseId;
	}
	public void setEntrepriseId(int entrepriseId) {
		this.entrepriseId = entrepriseId;
	}
	public String getDateEnvoie() {
		return this.dateEnvoie;
	}
	public void setDateEnvoie(String dateEnvoie) {
		this.dateEnvoie = dateEnvoie;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SubscriptionPK)) {
			return false;
		}
		SubscriptionPK castOther = (SubscriptionPK)other;
		return 
			(this.followerId == castOther.followerId)
			&& (this.entrepriseId == castOther.entrepriseId)
			&& this.dateEnvoie.equals(castOther.dateEnvoie);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.followerId;
		hash = hash * prime + this.entrepriseId;
		hash = hash * prime + this.dateEnvoie.hashCode();
		
		return hash;
	}
}